package edu.unk.fun330.base;

public class ShipJump extends ControllerAction {
	
	private float x;
	private float y;
	
	public ShipJump(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	public float getX() { return x; }
	public float getY() { return y; }

}
