package edu.unk.fun330;

import java.applet.AudioClip;
import java.awt.*;
import java.io.*;
import java.util.*;

public class Constants {
	
	public static boolean asApplet = false;
	public static Random rnd = new Random();
	public static final int width = Toolkit.getDefaultToolkit().getScreenSize().width;
	public static final int height = Toolkit.getDefaultToolkit().getScreenSize().height-40;
	
	public static final boolean SCALED_UNIVERSE = true;
	public static float scaleX = (float)Constants.width/Universe.WIDTH;
	public static float scaleY = (float)Constants.height/Universe.HEIGHT;
	
	public static final int numberOfAsteroids = 0; //2;

	public static final float asteroidRadius = 32.0f;
	//public static final int numberOfSplits = 3;
	
	public static final float maxShipSpeed = 5.0f;
	public static final float maxShipAccel = 1.0f;
	
	public static final boolean log = false; //true;
	public static Logger logWriter = new Logger();
	
	public static final boolean weapons = true;
	
	public static final boolean headsUpDisplay = true;
	public static final boolean showShipNames = true;
	public static final boolean SOUND = false;
	public static final boolean FAST_EXPLOSION = true;
	
	//public static final String centerShip = "Ashim";
	public static final String centerShip = "HumanPlayer";
	//public static final String centerShip = "none";
	//public static final String centerShip = "Bill";
	
	public static final int KILL_POINTS = 1000;
	public static final int RESPAWN_PENALTY = 200;
	
	public static final Color shieldColor = Color.GREEN;
	public static final Color fuelColor = Color.ORANGE;
	public static final Color bulletsColor = Color.RED;
	public static final Color livesColor = Color.YELLOW;
	public static final Color laserColor = Color.WHITE;
	public static final Color poisonColor = Color.MAGENTA;
	//public static final Color shieldColor = Color.GREEN;
	//public static final Color shieldColor = Color.GREEN;

	public static Image font;
	//public static Image explode;
	
	/*
	public static AudioClip audioHop, audioOuch, audioCarCrash, audioLevel;
	static {
	audioHop = getAudioClip(getCodeBase(), "sound/boing.au");
	audioOuch = getAudioClip(getCodeBase(), "sound/ouch.au");
	audioCarCrash = getAudioClip(getCodeBase(), "sound/carcrash.au");
	audioLevel = getAudioClip(getCodeBase(), "sound/newlevel.au");
	}
	*/
}
