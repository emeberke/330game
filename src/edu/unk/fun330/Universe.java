package edu.unk.fun330;

import java.awt.Graphics;
import java.util.*;

import edu.unk.fun330.base.*;
import edu.unk.fun330.base.effects.Cloak;
import edu.unk.fun330.base.effects.EffectCollection;
import edu.unk.fun330.base.effects.Exhaust;
import edu.unk.fun330.base.effects.Explosion;
import edu.unk.fun330.base.effects.Warp;



public class Universe {

	//private final transient Vector<FlyingObject> flyingObjects;

	private final transient Vector<ShipController> shipControllers;

	private final transient Collection<Ship> ships;
	private final transient Vector<Bullet> bullets;
	private final transient Vector<Asteroid> asteroids;
	private final transient Vector<AIShip> aiships;
	private final transient Vector<FlyingObject> sleepingObjects;
	private final transient Vector<FlyingObject> miscFlyingObjects;

	//private final transient Vector<Wall> walls;
	private EffectCollection<Exhaust> exhausts;  //was: private final transient Vector<Exhaust> exhausts;
	private EffectCollection<Explosion> explosions;  //was: private final transient Vector<Explosion> explosions;

	private final transient Vector<Warp> warps;
	private final transient Vector<Cloak> cloaks;

	private volatile transient Vector<String> displayText;

	public static final int WIDTH = 2400; //Constants.width;
	public static final int HEIGHT = 1600; //Constants.height;

	public static int viewPortXOffSet = 0;
	public static int viewPortYOffSet = 0;

	private boolean[] keyDown;

	private TreeSet<Ship> sortedShips;

	public Universe(){
		//flyingObjects = new Vector<FlyingObject>();

		ships = Collections.synchronizedSet( new HashSet<>() ); //HashSet is not synchronized for multithreading
		bullets = new Vector<>();
		asteroids = new Vector<>();
		aiships = new Vector<>();
		sleepingObjects = new Vector<>();
		miscFlyingObjects = new Vector<>();
		//walls = new Vector<Wall>();

		exhausts = new EffectCollection<>();
		explosions = new EffectCollection<>();
		warps = new Vector<>();
		cloaks = new Vector<>();

		shipControllers = new Vector<>();
		sortedShips = new TreeSet<>(new Comparator<Ship>() {
			@Override
			public int compare(Ship o1, Ship o2) {
				return o2.score - o1.score;
			}
		});
	}

	public void reset(){
		ships.clear();
		bullets.clear();
		asteroids.clear();
		aiships.clear();
		sleepingObjects.clear();
		miscFlyingObjects.clear();
		sortedShips.clear();
		//walls = new Vector<Wall>();

		//exaust.clear();
		//explosion.clear();
		exhausts = new EffectCollection<Exhaust>();
		explosions = new EffectCollection<Explosion>();
		warps.clear();
		cloaks.clear();

		shipControllers.clear();
	}

	protected void addSleeping(FlyingObject fo) {
		sleepingObjects.add(fo);
	}

	protected void removeSleeping(FlyingObject fo) {
		sleepingObjects.remove(fo);
	}

	protected void add(FlyingObject fo) {
		if (fo instanceof AIShip) aiships.add((AIShip)fo);  // do AIShip first since subclass of Ship
		else if (fo instanceof Ship) {
			ships.add((Ship)fo);
			sortedShips.add((Ship)fo);
		}
		else if (fo instanceof Bullet) bullets.add((Bullet)fo);
		else if (fo instanceof Asteroid) asteroids.add((Asteroid)fo);
		else miscFlyingObjects.add(fo);
	}

	protected void remove(FlyingObject fo) {
		if (fo instanceof AIShip) aiships.remove((AIShip)fo);
		else if (fo instanceof Ship) {
			ships.remove((Ship)fo);
			sortedShips.remove((Ship)fo);
		}
		else if (fo instanceof Bullet) bullets.remove((Bullet)fo);
		else if (fo instanceof Asteroid) asteroids.remove((Asteroid)fo);
		else miscFlyingObjects.remove(fo);
	}

	protected void addAll(Vector<FlyingObject> v) {
		for (FlyingObject fo : v) {
			this.add(fo);
		}
	}

	protected void removeAll(Vector<FlyingObject> v) {
		for (FlyingObject fo : v) {
			this.remove(fo);
		}
	}

	// following mess used so that the caller of getFlyingObjects can be determined
	// (safer than having the object pass in itself to getFlyingObjects)
	private static final CallerResolver CALLER_RESOLVER; // set in <clinit>

	// subclass SecurityManager in order to make getClassContext() accessible
	private static final class CallerResolver extends SecurityManager {
		protected Class [] getClassContext () { return super.getClassContext (); }
	}

	static {
		try {
			// fails if the current SecurityManager does not allow
			// RuntimePermission ("createSecurityManager"):
			CALLER_RESOLVER = new CallerResolver ();
		}
		catch (SecurityException se) {
			throw new RuntimeException ("ClassLoaderResolver: could not create CallerResolver: " + se);
		}
	}

	public Vector<FlyingObject> getFlyingObjects() {

		Class c = CALLER_RESOLVER.getClassContext()[2];
		//System.out.println("c = " + c);

		Vector<FlyingObject> flyingObjects = new Vector<FlyingObject>();
		flyingObjects.addAll(miscFlyingObjects); // add miscFlyingObjects first so that black holes are drawn behind ships

		flyingObjects.addAll(ships);
		flyingObjects.addAll(bullets);
		flyingObjects.addAll(asteroids);
		//flyingObjects.addAll(miscFlyingObjects); // add miscFlyingObjects first so that black holes are drawn behind ships


		// return only visible AI ships to the regular ship controllers
		if (c.getSuperclass().equals(ShipController.class)) {
			//System.out.println("non AI ship controller is asking");
			// add only visible ai ships for the regular ship controllers to see
			for (AIShip ai : aiships)
				if (!ai.isInvisible())
					flyingObjects.add(ai);
		}
		else {
			//System.out.println("other is asking");
			flyingObjects.addAll(aiships);
		}

		return flyingObjects;
		//return (Vector<FlyingObject>)(flyingObjects.clone());
	}
	
	/*
	protected Vector<Bullet> getBullets() {
		return bullets;
	}
	*/

	protected Vector<FlyingObject> getSleeping() {
		return sleepingObjects;
	}

	protected Vector<Ship> getShips() {
		Vector<Ship> shs = new Vector<Ship>();
		shs.addAll(ships);
		return shs;
	}

	protected Vector<AIShip> getAIShips() {
		Vector<AIShip> aishs = new Vector<AIShip>();
		aishs.addAll(aiships);
		return aishs;
	}

	/*
	protected Vector<Asteroid> getAsteroids() {
		return asteroids;
	}
	*/

	/*
	public Vector<Wall> getWalls() {
		return walls;
	}
	*/

	public Vector<String> getDisplayText() {
		return displayText;
	}

	public void setDisplayText(Vector<String> displayText) {
		this.displayText = displayText;
	}

	public Vector<ShipController> getShipControllers() {
		return shipControllers;
	}

	public boolean[] getKeyDown() {
		return keyDown;
	}

	public void setKeyDown(boolean[] keyDown) {
		this.keyDown = keyDown;
	}

	//public Vector<Exhaust> getExaust() {return exaust;}
	public void addExhaust(float x, float y) { exhausts.add(new Exhaust(x,y)); }
	public void paintExhausts(Graphics g) {
		exhausts.paint(g);
	}

	//public Vector<Explosion> getExplosion() {return explosion;}
	public void addExplosion(float x, float y) { explosions.add(new Explosion(x,y)); }
	public void paintExplosions(Graphics g) {
		explosions.paint(g);
	}

	public Vector<Warp> getWarp() {return warps;}

	public Vector<Cloak> getCloak() {return cloaks;}

	public static int getHeight() {
		return HEIGHT;
	}

	public static int getWidth() {
		return WIDTH;
	}

	public void setSortedShips(TreeSet<Ship> sortedShips) {
		this.sortedShips = sortedShips;
	}

	public TreeSet<Ship> getSortedShips() {
		return sortedShips;
	}

}
